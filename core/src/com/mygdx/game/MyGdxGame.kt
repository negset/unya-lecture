package com.mygdx.game

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.viewport.FitViewport
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.viewport.Viewport


const val WIDTH = 640f
const val HEIGHT = 480f

class MyGdxGame : ApplicationAdapter()
{
    private lateinit var viewport: Viewport
    private val camera = OrthographicCamera()

    private val touchPoint1 = Vector2()
    private val touchPoint2 = Vector2()

    private lateinit var batch: SpriteBatch

    private lateinit var player: Player
    private lateinit var enemy: Enemy
    private lateinit var ball: Ball

    private lateinit var font: BitmapFont
    private var playerScore = 0
    private var enemyScore = 0

    override fun create()
    {
        camera.setToOrtho(false, WIDTH, HEIGHT)
        viewport = FitViewport(WIDTH, HEIGHT, camera)

        batch = SpriteBatch()
        player = Player()
        enemy = Enemy()
        ball = Ball()
        font = BitmapFont()
    }

    override fun render()
    {
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        camera.update()
        batch.projectionMatrix = camera.combined

        if (Gdx.input.isTouched(0))
        {
            touchPoint1.set(Gdx.input.getX(0).toFloat(), Gdx.input.getY(0).toFloat())
            viewport.unproject(touchPoint1)
        }

        if (Gdx.input.isTouched(1))
        {
            touchPoint2.set(Gdx.input.getX(1).toFloat(), Gdx.input.getY(1).toFloat())
            viewport.unproject(touchPoint2)
        }

        batch.begin()
        player.draw(batch)
        enemy.draw(batch)
        ball.draw(batch)
        font.draw(batch, "$playerScore - $enemyScore",
                WIDTH / 2 - 10f, HEIGHT / 2)
        batch.end()

        ball.update()
        player.update(touchPoint1)
        enemy.update(touchPoint2)
        ball.collisionDetectionPlayer(player)
        ball.collisionDetectionEnemy(enemy)

        if (ball.x < 0)
        {
            enemyScore++
            ball = Ball()
        }
        else if (ball.x > WIDTH)
        {
            playerScore++
            ball = Ball()
        }
    }

    override fun resize(width: Int, height: Int)
    {
        viewport.update(width, height)
    }

    override fun dispose()
    {
        batch.dispose()
    }
}
