package com.mygdx.game

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.math.Vector2

open class Racket
{
    val width = 20f
    val height = 160f

    private val texture = Texture("racket.png")
    var x = 40f
    var y = 240f

    fun draw(batch: Batch)
    {
        batch.draw(texture, x - width / 2, y - height / 2)
    }

    open fun update(touchPoint: Vector2)
    {
    }

    fun moveUp()
    {
        y += 5f
        if (y > HEIGHT - height / 2)
            y = HEIGHT - height / 2
    }

    fun moveDown()
    {
        y -= 5f
        if (y < height / 2)
            y = height / 2
    }
}
