package com.mygdx.game

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.math.Vector2

class Ball
{
    private val width = 20f
    private val height = 20f

    private val texture = Texture("ball.png")

    var x = WIDTH / 2
    var y = HEIGHT / 2

    private val v = Vector2(-3.7f, 3.32f)

    private var counter = 0

    fun draw(batch: Batch)
    {
        batch.draw(texture, x - width / 2, y - height / 2)
    }

    fun update()
    {
        x += v.x
        y += v.y

        if (y < 0 + height / 2)
        {
            v.y = -v.y
            y = 0f + height / 2
        }
        else if (y > HEIGHT - height / 2)
        {
            v.y = -v.y
            y = HEIGHT - height / 2
        }

        if (++counter % 60 == 0)
            v.scl(1.1f)
    }

    fun collisionDetectionPlayer(player: Player)
    {
        val rBottom = player.y - player.height / 2
        val rTop = player.y + player.height / 2
        if (y !in rBottom..rTop)
            return

        val rx = player.x + player.width / 2
        val bx = x - width / 2

        if (bx < rx)
        {
            v.x = -v.x
            x = rx + width / 2
        }
    }

    fun collisionDetectionEnemy(enemy: Enemy)
    {
        val rBottom = enemy.y - enemy.height / 2
        val rTop = enemy.y + enemy.height / 2
        if (y !in rBottom..rTop)
            return

        val rx = enemy.x - enemy.width / 2
        val bx = x + width / 2

        if (bx > rx)
        {
            v.x = -v.x
            x = rx - width / 2
        }
    }
}
