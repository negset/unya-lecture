package com.mygdx.game

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.math.Vector2

class Player : Racket()
{
    override fun update(touchPoint: Vector2)
    {
        if (y in height / 2..HEIGHT - height / 2)
        {
            if (Gdx.input.isTouched(0) && touchPoint.x < WIDTH / 2)
                when (touchPoint.y)
                {
                    in 0f..y ->
                        moveDown()
                    in y..HEIGHT ->
                        moveUp()
                }
            else when
            {
                Gdx.input.isKeyPressed(Input.Keys.W) ->
                    moveUp()
                Gdx.input.isKeyPressed(Input.Keys.S) ->
                    moveDown()
            }
        }
    }
}
